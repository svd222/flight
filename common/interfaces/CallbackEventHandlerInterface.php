<?php
namespace common\interfaces;

interface CallbackEventHandlerInterface
{
    /**
     * @param $jsonObject
     * @return bool
     */
    public function handleEvent($jsonObject): bool;

    /**
     * @return bool
     */
    public function handleJob(): bool;
}
