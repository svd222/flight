<?php
namespace common\interfaces;

interface BookingInterface
{
    /**
     * Book place for passenger
     *
     * @param int $flightId
     * @param int $userId
     * @return bool
     */
    public function book(int $flightId, int $userId): bool;

    /**
     * Cancel reservation
     *
     * @param int $ticketId
     * @return bool
     */
    public function cancelReservation(int $ticketId): bool;
}
