<?php
namespace common\interfaces;

interface TicketInterface
{
    /**
     * Buy ticket
     *
     * @param int $flightId
     * @param int $userId
     * @return bool
     */
    public function buyTicket(int $flightId, int $userId): bool;

    /**
     * Return ticket
     *
     * @param int $ticketId
     * @return bool
     */
    public function returnTicket(int $ticketId): bool;
}
