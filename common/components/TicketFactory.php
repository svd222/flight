<?php
namespace common\components;

use common\models\Flight;
use common\models\Ticket;

class TicketFactory
{
    /**
     * @param $flightId
     * @param $userId
     * @return Ticket|null
     */
    public static function create($flightId, $userId)
    {
        $flight = Flight::findOne($flightId);
        if ($flight->getPlacesCount() > 0 && $flight->status === 0) {
            $ticket = new Ticket();
            $ticket->setAttributes([
                'user_id' => $userId,
                'flight_id'=> $flightId,
                'departure_date' => $flight->departure_date
            ]);
            $ticket->save();
            return $ticket;
        }
        return null;
    }
}
