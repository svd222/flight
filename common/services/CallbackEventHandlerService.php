<?php
namespace common\services;

use common\models\Flight;
use common\interfaces\CallbackEventHandlerInterface;
use common\components\RedisQueue;
use Yii;

class CallbackEventHandlerService implements CallbackEventHandlerInterface
{
    const FLIGHT_TICKET_SALES_COMPLETED = 'flight_ticket_sales_completed';

    const FLIGHT_CANCELED = 'flight_canceled';

    /**
     * @param $jsonObject
     * @return bool
     */
    public function handleEvent($jsonObject): bool
    {
        $flightId = $jsonObject['data']['flight_id'];
        $triggeredAt = $jsonObject['data']['triggered_at'];
        $event = $jsonObject['data']['event'];
        $secretKey = $jsonObject['data']['secret_key'];

        $flight = Flight::findOne($flightId);
        if (!$flight) {
            return false;
        }
        $tickets = $flight->tickets;
        if (($event) == self::FLIGHT_TICKET_SALES_COMPLETED) {
            $flight->status = 1;
            $flight->save();
            return true;
        }
        if (($event) == self::FLIGHT_CANCELED) {
            $flight->status = 2;

            $users = $flight->users;
            foreach ($users as $user) {
                $this->addJob($flightId, $user->email);
            }

            $flight->save();
            return true;
        }
        return false;
    }

    protected function addJob($flightId, $userEmail) {
        $message = 'Flight #' . $flightId . ' has been canceled';
        $jobData = [
            'message' => $message,
            'flightId' => $flightId,
            'userEmail' => $userEmail
        ];

        $queueName = Yii::$app->params['queue_canceled_flights'];
        $redisConfig = Yii::$app->params['redis_config'];

        try {
            // create queue
            $redisQueue = new RedisQueue($queueName, $redisConfig);

            while(True){
                // add to queue
                $index = $redisQueue->add($jobData);
                sleep(1);
            }
        } catch (RedisQueueException $e) {
            return false;
        }
        return true;
    }

    public function handleJob(): bool
    {
        $queueName = Yii::$app->params['queue_canceled_flights'];
        $redisConfig = Yii::$app->params['redis_config'];

        while (true){
            try {
                // create queue
                $redisQueue = new RedisQueue($queueName, $redisConfig);

                // fetch from queue
                $data = $redisQueue->get();

                $currentIndex = $redisQueue->getCurrentIndex($data);
                if (!empty($data)) {
                    extract($data);
                    $success = true;

                    if (isset($flightId, $message, $userEmail)) {
                        Yii::$app->mailer->compose()
                            ->setFrom('noreply@flight.local')
                            ->setTo($userEmail)
                            ->setSubject($message)
                            ->setTextBody($message)
                            ->send();
                    }
                } else {
                    $success = false;
                }
                sleep(1);

                if ($success) {
                    $ret = $redisQueue->remove($data);
                    if (!empty($ret)) {
                        return true;
                    }
                } else {
                    return false;
                    $redisQueue->rollback($data);  // if retry times up to max, the index will be transfer to blocked list
                }
            } catch (RedisQueueException $e) {
                return false;
            }
        }
        return true;
    }
}
