<?php
namespace common\services;

use common\components\TicketFactory;
use common\interfaces\BookingInterface;
use common\models\Ticket;
use common\models\UserFlight;

/**
 * Class BookingService
 *
 * @package common\services
 */
class BookingService implements BookingInterface
{
    /**
     * Book place for passenger
     *
     * @param int $flightId
     * @param int $userId
     * @return bool
     */
    public function book(int $flightId, int $userId): bool
    {
        $ticket = TicketFactory::create($flightId, $userId);
        $ticket->booked = 1;
        if (!$ticket->isReserved()) {
            $flight = $ticket->flight;
            $flight->places_number--;
            $flight->save();
        }
        return $ticket->save();
    }

    /**
     * Cancel reservation
     *
     * @param int $ticketId
     * @return bool
     */
    public function cancelReservation(int $ticketId): bool
    {
        $ticket = Ticket::findOne(['id' => $ticketId]);
        if ($ticket) {
            $ticket->booked = 0;
            if ($ticket->isReserved() && !$ticket->paid) {
                $flight = $ticket->flight;
                $flight->places_number++;
                $flight->save();
            }
            return $ticket->save();
        }
        return false;
    }
}
