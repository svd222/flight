<?php
namespace common\services;

use common\components\TicketFactory;
use common\interfaces\TicketInterface;
use common\models\Ticket;
use common\models\UserFlight;


/**
 * Class TicketService
 *
 * @package common\services
 */
class TicketService implements TicketInterface
{
    /**
     * Buy ticket
     *
     * @param int $flightId
     * @param int $userId
     * @return bool
     */
    public function buyTicket(int $flightId, int $userId): bool
    {
        $ticket = Ticket::findOne(['user_id' => $userId, 'flight_id' => $flightId]);
        if (!$ticket) {
            $ticket = TicketFactory::create($flightId, $userId);
        }
        if (!$ticket) {
            return false;
        }
        $ticket->paid = 1;
        if (!$ticket->isReserved()) {
            $flight = $ticket->flight;
            $flight->places_number--;
            $flight->save();
        }
        return $ticket->save();
    }

    /**
     * Return ticket
     *
     * @param int $ticketId
     * @return bool
     */
    public function returnTicket(int $ticketId): bool
    {
        $ticket = Ticket::findOne(['id' => $ticketId]);
        if ($ticket) {
            $ticket->booked = 0;
            if ($ticket->isReserved() && !$ticket->paid) {
                $flight = $ticket->flight;
                $flight->places_number++;
                $flight->save();
            }
            return $ticket->save();
        }
        return false;
    }
}
