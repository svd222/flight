##Installation

```
$ cd /path/to/project
$ git clone https://svd222@bitbucket.org/svd222/flight.git .
$ php yii init
$ composer install
```
### Install redis & php-redis
redis:

[redis](https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-18-04-ru)

```
$ sudo apt-get install php-redis
```

###Configuration

in a file /common/config/main-local.php we need configure `db` component

```php
'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=DATABASE_NAME',
            'username' => 'USERNAME',
            'password' => 'PASSWORD',
            'charset' => 'utf8',
            'tablePrefix' => 'fl_',
        ],
```

content of /common/config/params-local.php

```php 
    <?php
    return [
        'queue_canceled_flights' => 'canceled_flights',
        'redis_config' => [
            'host' => '127.0.0.1',
            'port' => '6379',
            'index' => '0'
        ],
    ];
```

next we are goin to run migrations:

```
$ php yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations
$ php yii migrate
```

Register some users through page
http://backend.flight.local/user/register

In mysql table `fl_user` please set `confirmed_at` field equal to `created_at` 
field if you don`t receive an email after user registration

The `fl_flight` table already has 5 flights

### API Routes & callback handler route

POST http://api.flight.local/v1/ticket/book params: user_id, flight_id

POST http://api.flight.local/v1/ticket/cancel-reservation params: ticket_id

POST http://api.flight.local/v1/ticket/buy-ticket params: user_id, flight_id

POST http://api.flight.local/v1/ticket/return-ticket params: ticket_id

POST http://api.flight.local/v1/callback/events params: raw json

```php
{"data":{"flight_id":1,"triggered_at":1585012345,"event":"flight_ticket_sales_completed","secret_key":"a1b2c3d4e5f6a1b2c3d4e5f6"}}
```

####Run queue to async handle of event

```bash
php yii queue/handle
```
After job handled appropriate field `status` in a table `fl_flight` 
should changed to 1 and appropriate users should receive an email

You can configure systemd or cronjob

###Nginx config example

```bash
   server {
   	listen 80;   	
   	sendfile        on;
   	keepalive_timeout  65;
    gzip  on;
    gzip_min_length 1024;
    gzip_buffers 12 32k;
    gzip_comp_level 9;
    gzip_proxied any;
   	gzip_types	text/plain application/xml text/css text/js text/xml application/x-javascript text/javascript application/javascript application/json application/xml+rss;
   	
   	server_name .flight.local;
   	root /var/www/flight/$alias/web;
   
   	access_log /var/www/flight/access.log;
   	error_log /var/www/flight/error.log;
   
   	charset utf-8;
   
   	set $alias  "frontend";
   
   	    if ($host ~* ^([a-z0-9-\.]+)\.flight.local$) {
   		set $subdomain $1;
   	    }
   
   	    if ($host ~* ^backend.flight.local$) {
   		set $alias  "backend";
   	    }
   	    if ($host ~* ^api.flight.local$) {
   		set $alias  "api";
   	    }
   
   		
   	location / {
   	    root /var/www/flight/$alias/web/;
   	    index index.php;
   	    try_files $uri $uri/ /index.php?$args;
   	}
   
       	location ~ \.php$ {
   		fastcgi_split_path_info ^(.+\.php)(/.+)$;
   		
   		root /var/www/flight/$alias/web/;
   
   		try_files $uri $uri/ =404;
   		fastcgi_pass unix:/run/php/php7.3-fpm.sock;
   		fastcgi_index index.php;
   		include fastcgi_params;
   		fastcgi_param                   SCRIPT_FILENAME $document_root$fastcgi_script_name;
   
   
   	    	fastcgi_param QUERY_STRING    $query_string;
   	    	fastcgi_param REQUEST_METHOD  $request_method;
   	    	fastcgi_param CONTENT_TYPE    $content_type;
   	    	fastcgi_param CONTENT_LENGTH  $content_length;
   		fastcgi_param  PATH_INFO $fastcgi_path_info;
       	}
   
   }
```
