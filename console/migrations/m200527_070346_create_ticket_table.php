<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ticket}}`.
 */
class m200527_070346_create_ticket_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ticket}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'flight_id' => $this->integer()->notNull(),
            'booked' => $this->tinyInteger()->notNull()->defaultValue(0),
            'paid' => $this->tinyInteger()->notNull()->defaultValue(0),
            'created_at' => $this->timestamp()->notNull(),
            'departure_date' => $this->dateTime()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->addForeignKey('FK_user', '{{%ticket}}', 'user_id', '{{%user}}', 'id');
        $this->addForeignKey('FK_flight', '{{%ticket}}', 'flight_id', '{{%flight}}', 'id');
        $this->createIndex('K_ticket_booked', '{{%ticket}}', 'booked');
        $this->createIndex('K_ticket_paid', '{{%ticket}}', 'paid');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ticket}}');
    }
}
