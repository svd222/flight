<?php

use yii\db\Migration;

/**
 * Class m200527_115555_fill_flight_table
 */
class m200527_115555_fill_flight_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->getDb()->createCommand('INSERT INTO {{%flight}}
            (created_at, departure_date, arrival_date, flight_code, status, places_number) 
            VALUES
            (CURRENT_TIMESTAMP, \'2020-07-28 13:50:00\', \'2020-07-28 16:50\', \'FL-12\', 0, 120),
            (CURRENT_TIMESTAMP, \'2020-07-29 13:20:00\', \'2020-07-29 16:40\', \'FL-13\', 0, 100),
            (CURRENT_TIMESTAMP, \'2020-07-28 08:50:00\', \'2020-07-28 12:10\', \'FL-15\', 0, 120),
            (CURRENT_TIMESTAMP, \'2020-07-28 13:50:00\', \'2020-07-28 16:50\', \'FL-18\', 0, 150),
            (CURRENT_TIMESTAMP, \'2020-07-28 13:00:00\', \'2020-07-28 14:00\', \'FL-19\', 0, 140),
            (CURRENT_TIMESTAMP, \'2020-07-28 13:50:00\', \'2020-07-28 16:50\', \'FL-22\', 0, 120)
            ;')
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200527_115555_fill_flight_table cannot be reverted.\n";

        return false;
    }
}
