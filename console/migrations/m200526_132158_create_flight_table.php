<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%flight}}`.
 */
class m200526_132158_create_flight_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%flight}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->timestamp()->notNull(),
            'departure_date' => $this->dateTime()->notNull(),
            'arrival_date' => $this->dateTime()->notNull(),
            'flight_code' => $this->string(12)->notNull(),
            'status' => $this->tinyInteger()->notNull()->defaultValue(0),
            'places_number' => $this->smallInteger()->notNull()->defaultValue(150),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->createIndex('i_flight_flight_code', '{{%flight}}', 'flight_code');
        $this->createIndex('i_flight_status', '{{%flight}}', 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%flight}}');
    }
}
