<?php
namespace console\controllers;

use common\services\CallbackEventHandlerService;
use yii\console\Controller;
use Yii;

class QueueController extends Controller
{
    public function actionHandle()
    {
        $service = Yii::$container->get(CallbackEventHandlerService::class);
        $service->handleJob();
    }
}
