<?php
Yii::$container->setDefinitions([
    \common\interfaces\BookingInterface::class => \common\services\BookingService::class,
    \common\interfaces\TicketInterface::class => \common\services\TicketService::class
]);
