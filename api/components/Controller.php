<?php
namespace api\components;

use yii\web\Controller as WebController;
use Yii;
use yii\web\Response;

class Controller extends WebController
{
    public function beforeAction($action)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->statusCode = 200;
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
}
