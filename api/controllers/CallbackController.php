<?php
namespace api\controllers;

use api\components\Controller;
use common\interfaces\CallbackEventHandlerInterface;
use common\services\CallbackEventHandlerService;
use yii\helpers\Json;
use Yii;

/**
 * Class CallbackController
 *
 * @package api\controllers
 */
class CallbackController extends Controller
{
    /**
     * @return string[]
     */
    public function actionEvents()
    {
        $jsonData = Json::decode(Yii::$app->request->getRawBody());
        /**
         * @var CallbackEventHandlerInterface $service
         */
        $service = Yii::$container->get(CallbackEventHandlerService::class);
        if ($service->handleEvent($jsonData)) {
            return [
                'status' => 'success',
            ];
        } else {
            return [
                'status' => 'fail',
            ];
        }
    }
}
