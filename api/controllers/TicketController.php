<?php
namespace api\controllers;

use api\components\Controller;
use common\interfaces\BookingInterface;
use common\interfaces\TicketInterface;
use Yii;
use yii\helpers\Json;


/**
 * Class TicketController
 *
 * @package api\controllers
 */
class TicketController extends Controller
{
    /**
     * Book place for passenger
     *
     * @return string[]
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionBook()
    {
        $post = Yii::$app->request->post();
        $userId = $post['user_id'];
        $flightId = $post['flight_id'];
        /**
         * @var BookingInterface $service
         */
        $service = Yii::$container->get(BookingInterface::class);
        if ($service->book($flightId, $userId)) {
            return [
                'status' => 'success',
            ];
        } else {
            return [
                'status' => 'fail',
            ];
        }
    }

    /**
     * Cancel reservation
     *
     * @return string[]
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCancelReservation()
    {
        $post = Yii::$app->request->post();
        $ticketId = $post['ticket_id'];
        /**
         * @var BookingInterface $service
         */
        $service = Yii::$container->get(BookingInterface::class);
        if ($service->cancelReservation($ticketId)) {
            return [
                'status' => 'success',
            ];
        } else {
            return [
                'status' => 'fail',
            ];
        }
    }

    /**
     * Buy ticket
     *
     * @return string[]
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionBuyTicket()
    {
        $post = Yii::$app->request->post();
        $userId = $post['user_id'];
        $flightId = $post['flight_id'];
        /**
         * @var TicketInterface $service
         */
        $service = Yii::$container->get(TicketInterface::class);
        if ($service->buyTicket($flightId, $userId)) {
            return [
                'status' => 'success',
            ];
        } else {
            return [
                'status' => 'fail',
            ];
        }
    }

    /**
     * Return ticket
     *
     * @return string[]
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionReturnTicket()
    {
        $post = Yii::$app->request->post();
        $ticketId = $post['ticket_id'];
        /**
         * @var TicketInterface $service
         */
        $service = Yii::$container->get(TicketInterface::class);
        if ($service->returnTicket($ticketId)) {
            return [
                'status' => 'success',
            ];
        } else {
            return [
                'status' => 'fail',
            ];
        }
    }
}




















